package com.nesrine.users.boot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.nesrine.users.model.Utilisateur;
import com.nesrine.users.repository.UtilisateurRepository;

@Component
public class DataLoader implements CommandLineRunner  {
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
		public void setUtilisateurRepository(UtilisateurRepository utilisateurRepository) {
		this.utilisateurRepository = utilisateurRepository;
	}

	@Override
	public void run(String... arg0) throws Exception {
		System.out.println("*************test run*************");
		Utilisateur utilisateur1= new Utilisateur();
		utilisateur1.setNom("toto");
		utilisateur1.setPrenom("titi");
		utilisateur1.setEmail("toto@gmail.com");
		utilisateur1.setAdresse("Tunis");
		utilisateur1.setPhone("1234565445");
		utilisateur1.setIdentifiant("254555");
		utilisateur1.setPassword("4578787878");
		
utilisateurRepository.save(utilisateur1);

       Utilisateur utilisateur2= new Utilisateur();
       utilisateur2.setNom("nesrine");
       utilisateur2.setPrenom("zaiene");
       utilisateur2.setEmail("zaiene@gmail.com");
       utilisateur2.setAdresse("Monastire");
       utilisateur2.setPhone("1254545454");
       utilisateur2.setIdentifiant("44444444");
       utilisateur2.setPassword("14545444");

utilisateurRepository.save(utilisateur2);
		
	}
	

}
