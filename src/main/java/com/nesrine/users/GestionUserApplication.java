package com.nesrine.users;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionUserApplication {
	

	public static void main(String[] args) {
		SpringApplication.run(GestionUserApplication.class, args);
	}

	
}
