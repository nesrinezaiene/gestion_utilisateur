package com.nesrine.users.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nesrine.users.dto.StringResponse;
import com.nesrine.users.model.Utilisateur;
import com.nesrine.users.service.UtilisateurService;
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/utilisateur")
public class UtilisateurController {
	@Autowired
	private UtilisateurService utilisateurService;
	
	
	public void setUtilisateurService(UtilisateurService utilisateurService) {
		this.utilisateurService = utilisateurService;
	}

	@GetMapping("/list")
	public List<Utilisateur> findAll() {
		try {
			return utilisateurService.read();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
			}
	
	@GetMapping("/basic/get/{id}")
	public Utilisateur getOneUserBasic(@PathVariable("id") int id) {
		try {
			return utilisateurService.read(id);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	@PostMapping("/add")
	public StringResponse add(@RequestBody Utilisateur utilisateur) {
		try {
			return utilisateurService.create(utilisateur);
		} catch (Exception e) {
			e.printStackTrace();
			return new StringResponse(false, "Ajout non effectué");
		}
	}
	
	@PutMapping("/update")
	public StringResponse update(@RequestBody Utilisateur utilisateur) {
		try {
			return utilisateurService.update(utilisateur);
		} catch (Exception e) {
			e.printStackTrace();
			return new StringResponse(false, "Modification non effectué");
		}
	}
	
}
