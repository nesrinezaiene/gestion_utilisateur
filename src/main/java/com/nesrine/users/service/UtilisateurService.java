package com.nesrine.users.service;

import java.util.List;

import com.nesrine.users.dto.StringResponse;
import com.nesrine.users.model.Utilisateur;

public interface UtilisateurService {
	
	public StringResponse create(Utilisateur utilisateur) throws Exception;
	public StringResponse update(Utilisateur utilisateur) throws Exception;
	public StringResponse delete(int id) throws Exception;
	public Utilisateur read(int id) throws Exception;
	public List<Utilisateur> read() throws Exception;
	
	
}
