package com.nesrine.users.service.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nesrine.users.dto.StringResponse;
import com.nesrine.users.model.Utilisateur;
import com.nesrine.users.repository.UtilisateurRepository;
import com.nesrine.users.service.UtilisateurService;
@Service
public class UtilisateurServiceImpl implements UtilisateurService{
	@Autowired
	private UtilisateurRepository  utilisateurRepository; 
	
	public void setUtilisateurRepository(UtilisateurRepository utilisateurRepository) {
		this.utilisateurRepository = utilisateurRepository;
	}
	@Transactional
	@Override
	public StringResponse create(Utilisateur utilisateur) throws Exception {
		utilisateurRepository.save(utilisateur);
		return new StringResponse(true, "Ajout effecué");
	}
	@Transactional
	@Override
	public StringResponse update(Utilisateur utilisateur) throws Exception {
		
		utilisateurRepository.save(utilisateur);
		return new StringResponse(true, "Modification effecué");
	}
	@Transactional
	@Override
	public StringResponse delete(int id) throws Exception {
		utilisateurRepository.delete(id);
		return new StringResponse(true, "Suppression effecué");
	}
	@Transactional
	@Override
	public Utilisateur read(int id) throws Exception {
		return utilisateurRepository.findOne(id);
	}
	@Transactional
	@Override
	public List<Utilisateur> read() throws Exception {
		
		return utilisateurRepository.findAll();
	}

}
